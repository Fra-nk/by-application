# Deployment

* Install [Ansible](https://www.ansible.com/) on a central machine.
* Set up connected systems.
    * Make sure a SSH server is installed.
    * Copy your SSH key by using `ssh-copy-id`
    * Depending on the configuration you should probably adapt `/etc/sudoers`.
      This way you can use `sudo` for certain commands without entering a password.
* Adapt `hosts` & `image_downloader.yml` accordingly:
    * _root_ login is probably disabled, but `sudo` can be used.
    * `vars:` contains all the system customization like URL for the web-server, folders to use, etc.
* Run `ansible-playbook image_downloader.yml -i hosts [--ask-become-pass]` from the central machine.

Apache setup is performed by [mrlesmithjr's ansible role](https://github.com/mrlesmithjr/ansible-apache2).
The role is included as a git submodule.
